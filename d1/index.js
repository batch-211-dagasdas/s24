console.log('helo world');


// ES6 update

// is one of the latest version of writing JS and in fact is one of the ajor update in js
// let /const- are es6 update, the are the new standard of creating variables

// exponent Operators



const secondNum = Math.pow(8,2);
console.log(secondNum);


let string1 = 'fun';
let string2 = 'bootcamp';
let string3 = 'coding';
let string4 = 'JavaScript';
let string5 = 'zuit';
let string6 = 'Learning';
let string7 = 'love';
let string8 = 'I';
let string9 = 'is';
let string10 = 'In';


let sentence1 = string8+" " + string7+ " " + string6 +" "+ string3;
let sentence2 = string5 +" "+ string3 +" "+ string2 +" "+ string9 +" "+ string1;
console.log(sentence1);
console.log(sentence2);

// template literals
	// allow us to cre8 sting using `` and easinly embed js expression in it
	/*
		it allows us to write str w/out using the concatenation operation
		(+)
		greatly help w/ code readability
	*/

let sentence3 = `${string8} ${string7} ${string6} ${string3} `;
console.log(sentence3)

/*
	${} is a placeholder that us used to embed Js expression  when creating str using template literals
*/
// pre template literal string use ( "" or  '')
let name = 'rap'; 
let message = "hello" +name+' ! welcome to programing!';
console.log(message)
// string using template literals
// (``) backticks
message =`Hello ${name}! Welcome to programming.`;
console.log(message)

const firstNum= 8**2;
console.log(firstNum);64
// multi line using template literals
const anotherMessage = `${name} attendedn a math competition..
 he won it by solving the problem 8**2 w/ the solution of ${firstNum}`
console.log(anotherMessage);


let dev ={
name: 'petter',
lastName: 'parket',
occupation: 'web developer',
income: 50000,
expenses: 60000
};

console.log(`${dev.name} is a ${ dev.occupation}.`);
console.log(`his income is ${dev.income} and expenses at ${ dev.expenses}. his current balance is ${dev.income - dev.expenses}`);

const interestRate = .1;
const principal = 1000;

console.log(` The interest on your saving account is 
	${principal* interestRate}`)


// Array Destructuring
/*
	-allow us to unpack elements in array into distinct var

	syntax:
	let/const [variableNameA,variableNameB,variableNameC] =array;

*/

const fullName =['juan','dela', 'crus'];

// pre-array destructing
console.log(fullName[0]);console.log(fullName[1]);console.log(fullName[2]);

console.log(`hello ${fullName[0]} ${fullName[1]} ${fullName[2]} ! it is nice to meet you!`)


// array Destructuring

const[ firstName ,middleName, lastName] =fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`hello ${firstName} ${middleName} ${lastName} ! it is nice to meet you!`);


// object Destructuring
/*
	allow us to unpack properties of object into distinct variables

	syntax
	let/const {propertNameA,propertNameB,propertNameC}= object
	
*/

const person ={
	givenName: 'jane',
	maidenName: 'dela',
	familyName: 'cruz'
};
// pre-object destrucuring
// we can acces them using . or []
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
// object Destructuring
const{givenName, maidenName ,familyName} =person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

// arrow function 
/*
	-compact alternative syntax to traditional functions
*/

const hello = () => {
	console.log("helo world");
}

hello();

// pre-arrow function and template literals
/*
	syntax
	function functionName(parametersA, parameterB){
	Statements;
	}
*/

// arrow function 
/*
	syntax: 
	let/const variableName = ( parameterA,b,c) =>{
	statment;
	}
*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
};
printFullName("ralpg", 'd','dagasdas');


// arrow function w/ loops
	// pre-arrow function 
	const student= ["john", 'jane','judy']

student.forEach(function(student){
	console.log(`${student} is a student`);
})
// arrow function
student.forEach((student)=>{
	console.log(`${student} is a student.`)
});

// Implicit return statement

/*
	there are instances when you can omit the "return" statement
	this works bec even w/out the 'return' statement js implicitly add it for the result of function
*/

// // pre-arrow function
// const add = (x,y) => {
// 	return x+y;
// }
// let total =add(1,2);
// console.log(total)


	// {} in an arrow function are code block. if an arrow function has a {} or code block, were going to nee to use "return"
	// implicit retun will only work on arrow function w/ out {}
const add = (x,y) => x+y;
let total =add(1,2);
console.log(total)


const sub = (x,y) => x-y;
let totalSub = sub(1,2);
console.log(totalSub)

const multi = (x,y) => x*y;
let totalmulti = sub(1,2);
console.log(totalmulti)


const divide = (x,y) => x/y;
let totaldiv = divide(1,2);
console.log(totaldiv)

//  dfault function arg value
// provide deflt arg bvalue if none is provided whein the function us invoked

const greet =(name = 'User') => {
	return `Good evening, ${name}`;

};
console.log(greet());
console.log(greet('john'));


//  class-based object bluepring
/*
	allows creating / instantiation of objectio using classes as blueprint
*/

// creating clas
/*
	the constructor is a special method of a clas for creating/initializing an object for the class

	syntax
	class className{
	constructor(objectPropertyA,objectPropertyB){
		this.objectPropertyA =objectPropertyA;
		this.objectPropertyB = objectPropertB,
		}
	}
*/

class Car{
	constructor(brand,name,year){
		this.brand=brand;
		this.name=name;
		this.year=year;
	}
}
// instantiating an object
/*
	-The 'new' operator creates/instantiate a new object w/ the given argument as the value of its properties
*/
	//  let/const variableName =new className();
		/*
			creating a constant w/ the "const" keyword and assigning it a value of an object makes it so we cant reassign it w/ another data type
		*/

const myCar = new Car();

console.log(myCar);

myCar.brand = 'ford';
myCar.name = "everest"
myCar.year =" 2015"
console.log(myCar);

//  or 

const myNewCar = new Car('toyota','vios',2022)
console.log(myNewCar);

// 

//  mini activity
class character{
	constructor(name,role,strength,weakness){
		this.name=name;
		this.role=role;
		this.strength=strength;
		this.weakness=weakness;
	}
}

const me = new character('toyota','vios',2022)
console.log(me);

